import { D1Orm, DataTypes, Model } from "d1-orm";

function getPromptModel(orm_instance) {
    const prompt = new Model(
        {
            D1Orm: orm_instance,
            tableName: "prompts",
            primaryKeys: "uuid",
            autoIncrement: false,
            uniqueKeys: [["uuid"]],
        },
        {
            uuid: {
                type: DataTypes.STRING,
                notNull: true,
            },
            prompt: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            lang: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            generations: {
                type: DataTypes.TEXT,
                notNull: true,
                defaultValue: "",
            },
            tags: {
                type: DataTypes.TEXT,
                notNull: true,
                defaultValue: "",
            },
        }
    );
    return prompt;
}

function getMatchModel(orm_instance) {
    const match = new Model(
        {
            D1Orm: orm_instance,
            tableName: "match",
            primaryKeys: "uuid",
            autoIncrement: false,
            uniqueKeys: [["uuid"]],
        },
        {
            uuid: {
                type: DataTypes.STRING,
                notNull: true,
            },
            prompt: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            session: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            
        }
    );
    return match;
}

function getRankModel(orm_instance) {
    const rank = new Model(
        {
            D1Orm: orm_instance,
            tableName: "rank",
            primaryKeys: "uuid",
            autoIncrement: false,
            uniqueKeys: [["uuid"]],
        },
        {
            uuid: {
                type: DataTypes.STRING,
                notNull: true,
            },
            match: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            winner: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
            elapsed_time: {
                type: DataTypes.REAL,
                notNull: true,
                defaultValue: "",
            },
            
        }
    );
    return rank;
}