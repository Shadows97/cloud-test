import { sqliteTable, text, integer, uniqueIndex } from 'drizzle-orm/sqlite-core';
import { drizzle } from 'drizzle-orm/d1';
 
export const users = sqliteTable('users', {
    id: integer('id').primaryKey(),
    name: text('name'),
  }
);


export async function onRequestGet(context) {
    const db = drizzle(context.env.DB);
    const result = await db.select().from(users).all()
    return new Response(JSON.stringify(result, null, 2), { status: 200 });
}


 