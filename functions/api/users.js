import { D1Orm, DataTypes, Model } from "d1-orm";

function getUserModel(orm_instance){
    const users = new Model(
        {
            D1Orm: orm_instance,
            tableName: "users",
            primaryKeys: "id",
            autoIncrement: "id",
            uniqueKeys: [["id"]],
        },
        {
            id: {
                type: DataTypes.INTEGER,
                notNull: true,
            },
            name: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
        }
    );
    return users
}


export async function onRequestGet(context) {
    const orm = new D1Orm(context.env.DB);
    const users = getUserModel(orm)
    const res = await users.All({
        where: {},
        limit: 10,
        offset: 0,
        orderBy: ["id"],
    });
    return new Response(JSON.stringify(res, null, 2), { status: 200 });
}

export async function onRequestPost(context) {
    const orm = new D1Orm(context.env.DB);
    const users = getUserModel(orm)
    // try {
        console.log("first")
    let input = await context.request.json();
    console.log(JSON.stringify(input))
    const result = await users.InsertOne(input)
    return new Response(JSON.stringify(result, null, 2), { status: 201 });
    //   } catch (err) {
    //     return new Response(JSON.stringify(err, null, 2), { status: 400 });
    //   }
}