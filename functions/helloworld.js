import { D1Orm, DataTypes, Model } from "d1-orm";

// We must initialise an ORM to use the Model class. This is done by passing in a D1Database instance (in this case it's bound to the `env.DB` environment variable).


// Now, to create the model:
function getUserModel(orm_instance){
    const users = new Model(
        {
            D1Orm: orm_instance,
            tableName: "users",
            primaryKeys: "id",
            autoIncrement: "id",
            uniqueKeys: [["id"]],
        },
        {
            id: {
                type: DataTypes.INTEGER,
                notNull: true,
            },
            name: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "",
            },
        }
    );
    return users
}





export async function onRequest(context) {
    const orm = new D1Orm(context.env.DB);
    const users = getUserModel(orm)
    // const result = await users.InsertOne({
    //     name: 'John Doe',
    // })
    const res = await users.All({
        where: {},
        limit: 10,
        offset: 0,
        orderBy: ["id"],
    });
    return new Response(JSON.stringify(res, null, 2));
}