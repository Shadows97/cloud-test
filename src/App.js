import logo from './logo.svg';
import './App.css';
import React, { useEffect } from 'react';

function App() {

  useEffect(() => {
    const getPosts = async () => {
        const resp = await fetch('/api/users', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ name: 'React POST Request Example' })
      });
        const postsResp = await resp.json();
        console.log(postsResp)
    };

    getPosts();
}, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
